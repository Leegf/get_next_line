/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/06 18:28:11 by lburlach          #+#    #+#             */
/*   Updated: 2017/12/15 20:31:14 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static	void	delete_list(void *content, size_t content_size)
{
	free(content);
	content = NULL;
	content_size = 0;
}

static	char	*allocate(t_list *tmp, char **line)
{
	char	*rez;
	t_list	*beg;
	int		i;
	size_t	c;

	c = 0;
	beg = tmp;
	rez = (*line);
	while (tmp)
	{
		c += tmp->content_size;
		tmp = tmp->next;
	}
	rez = ft_strnew(c);
	i = 0;
	while (beg)
	{
		ft_memmove(&rez[i], (const char *)beg->content, beg->content_size);
		i += (beg->content_size);
		beg = beg->next;
	}
	return (rez);
}

static	void	init(t_ffs **hm, int flag, size_t l, int fd)
{
	if (flag)
	{
		(*hm)->fd = fd;
		(*hm)->line_l = l;
		(*hm)->j = 0;
		return ;
	}
	(*hm) = malloc(sizeof(t_ffs));
	(*hm)->st_list = NULL;
	(*hm)->j = 0;
	(*hm)->line_l = 0;
}

static	int		read_line(t_ffs **hm, t_list **tmp, int fd, char **line)
{
	long long	ret;
	char	buf[BUFF_SIZE];
	size_t	i;
	void	*data;

	i = 0;
	while ((ret = read(fd, buf, BUFF_SIZE)))
	{
		if (ret == -1)
			return (-1);
		if ((data = ft_memchr((const void *)buf, '\n', ret)))
		{
			i = data - (void *)buf;
			ft_lst_push_back(tmp, buf, i);
			(*line) = allocate((*tmp), line);
			if (ret - i - 1 > 0)
			{
				if (!(*hm))
					init(hm, 0, 0, 0);
				ft_lst_push_front(&((*hm)->st_list), &buf[i + 1], ret - i - 1);
				init(hm, 1, ret - i - 1, fd);
				ft_lstdel(tmp, &delete_list);
			}
			return (1);
		}
		ft_lst_push_back(tmp, buf, ret);
	}
	if (ret <= 0 && (*tmp))
	{
//		if ((*hm)/* && (*hm)->line_l != 0 */ /* && !ft_memchr((*hm)->st_list->content, '\n', (*hm)->st_list->content_size) */)
//		{
//			return (0);
//		}	
 	if (((*tmp)->content_size) != 0 )
		{
			(*line) = allocate((*tmp), line); 
			ft_lstdel(tmp, &delete_list);
			return (1);
		}
	}
	return (ret <= 0) ? 0 : 1;
}

int		get_next_line(const int fd, char **line)
{
	int				i;
	static	t_ffs	*hm = NULL;
	t_list			*tmp;
	t_ffs			*hm2;

	i = 0;
	hm2 = hm;
	tmp = NULL;
	if (fd < 0 || !line)
		return (-1);
	while (hm)
	{
		if ((hm->fd) == fd)
		{
			i = hm->j;
			while (hm->line_l > 0)
			{
				if (((char *)(st_listt->content))[(hm->j)] == '\n')
				{
					ft_lst_push_back(&tmp, &(st_listt->content)[i], hm->j - i);
					hm->line_l--;
					hm->j++;
					(*line) = allocate(tmp, line);
					ft_lstdel(&tmp, &delete_list);
					hm = hm2;
					return (1);
				}
				hm->line_l--;
				hm->j++;
			}
			ft_lst_push_back(&tmp, &(st_listt->content)[i], (hm->j) - i);
			hm = hm2;
			break ;
		}
		hm = hm->next;
	}
		return (read_line(&hm, &tmp, fd, line));
}
