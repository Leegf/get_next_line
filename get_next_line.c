/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/17 13:01:01 by lburlach          #+#    #+#             */
/*   Updated: 2017/12/26 16:16:03 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static	int		ftt_lst_clear(t_list **alst)
{
	t_list *tmp;

	if (!alst || !(*alst))
		return (1);
	while (*alst)
	{
		tmp = *alst;
		(*alst) = (*alst)->next;
		free(tmp->content);
		tmp->content = NULL;
		free(tmp);
		tmp = NULL;
	}
	*(alst) = NULL;
	return (1);
}

static	char	*allocate(t_list *tmp, char **line)
{
	t_list			*beg;
	size_t			i;
	size_t			c;

	c = 0;
	beg = tmp;
	while (beg)
	{
		c += beg->content_size;
		beg = beg->next;
	}
	(*line) = ft_memalloc(c + 1);
	i = 0;
	while (tmp)
	{
		ft_memcpy(&(*line)[i], tmp->content, tmp->content_size);
		i += (tmp->content_size);
		tmp = tmp->next;
	}
	return ((*line));
}

static	void	ft_pus(t_ffs **cp, char *buf, size_t s, int fd)
{
	t_ffs *tmp;

	tmp = malloc(sizeof(t_ffs));
	tmp->d = ft_memalloc(s + 1);
	tmp->s = s;
	tmp->d = ft_memcpy(tmp->d, buf, s);
	tmp->pos = 0;
	tmp->fd = fd;
	if (!(*cp))
	{
		*cp = tmp;
		return ;
	}
	tmp->next = *cp;
	*cp = tmp;
}

static	int		read_lines(t_ffs **cp, t_list **tmp, int fd, char **line)
{
	int				ret;
	char			buf[BUFF_SIZE + 1];
	void			*data;

	while ((ret = read(fd, buf, BUFF_SIZE)))
	{
		buf[ret] = '\0';
		if (ret == -1)
			return (-1);
		if ((data = ft_strchr(buf, '\n')) != NULL)
		{
			ft_lst_push_back(tmp, buf, DAT);
			(*line) = allocate((*tmp), line);
			if (ret - DAT - 1 >= 0)
				ft_pus(cp, &(buf[DAT + 1]), ret - DAT - 1, fd);
			return (ftt_lst_clear(tmp));
		}
		ft_lst_push_back(tmp, buf, ret);
	}
	fd = (*tmp) ? -42 : 0;
	if (ret <= 0 && fd == -42)
		(*line) = allocate((*tmp), line);
	if (*cp)
		(*cp)->s = (*cp)->pos;
	return ((ftt_lst_clear(tmp) && ret <= 0 && fd == -42) || (ret > 0)) ? 1 : 0;
}

int				get_next_line(const int fd, char **line)
{
	static	t_ffs	*s_head = NULL;
	t_list			*tmp;
	t_ffs			*cp;
	char			*t;

	cp = s_head;
	tmp = NULL;
	while (cp)
	{
		if (cp->fd == fd)
		{
			if ((t = ft_strchr(&(CR(cp->d))[CP], '\n')) != NULL)
			{
				ft_lst_push_back(&tmp, &(CR(cp->d))[CP], t - &(CR(cp->d))[CP]);
				CP = CP + (t - (&(CR(cp->d))[CP])) + 1;
				(*line) = allocate(tmp, line);
				return (ftt_lst_clear(&tmp));
			}
			if ((cp->s - CP) > 0)
				ft_lst_push_back(&tmp, &(CR(cp->d))[CP], cp->s - CP);
			break ;
		}
		cp = cp->next;
	}
	return (fd < 0 || fd > 4096 || !line) ? -1 : READ_L;
}
