/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main4.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/22 13:52:29 by lburlach          #+#    #+#             */
/*   Updated: 2017/12/22 14:03:54 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

int		main(int argc, char **argv)
{
	char *line;
	int fd;

	fd = open(argv[argc - 1], O_RDONLY);
	while (get_next_line(fd, &line))
	{
		ft_putendl(line);
		ft_memdel((void **)&line);
	}

/*	char *line;
	int		out;
	int		p[2];
	int		fd;
	int		gnl_ret;

	fd = 1;
	out = dup(fd);
	pipe(p);
	dup2(p[1], fd);
	write(fd, "aaa", 3);
	close(p[1]);
	dup2(out, fd);
	gnl_ret = get_next_line(p[0], &line);
	ft_putnbr(strcmp(line, "aaa") == 0);
	*/
	/*
		char 	*line;
	int		out;
	int		p[2];
	int		fd;
	int		ret;

	out = dup(1);
	pipe(p);

	fd = 1;
	dup2(p[1], fd);
	write(fd, "abcdefghijklmnop\n", 17);
	write(fd, "qrstuvwxyzabcdef\n", 17);
	write(fd, "ghijklmnopqrstuv\n", 17);
	write(fd, "wxyzabcdefghijkl\n", 17);
	write(fd, "mnopqrstuvwxyzab\n", 17);
	write(fd, "cdefghijklmnopqr\n", 17);
	write(fd, "stuvwxzabcdefghi\n", 17);
	close(p[1]);
	dup2(out, fd);
	get_next_line(p[0], &line);
	ft_putnbr(strcmp(line, "abcdefghijklmnop") == 0);
	get_next_line(p[0], &line);
	ft_putnbr(strcmp(line, "qrstuvwxyzabcdef") == 0);
	get_next_line(p[0], &line);
	ft_putnbr(strcmp(line, "ghijklmnopqrstuv") == 0);
	get_next_line(p[0], &line);
	ft_putnbr(strcmp(line, "wxyzabcdefghijkl") == 0);
	get_next_line(p[0], &line);
	ft_putnbr(strcmp(line, "mnopqrstuvwxyzab") == 0);
	get_next_line(p[0], &line);
	ft_putnbr(strcmp(line, "cdefghijklmnopqr") == 0);
	get_next_line(p[0], &line);
	ft_putnbr(strcmp(line, "stuvwxzabcdefghi") == 0);
	ret = get_next_line(p[0], &line);
	ft_putnbr(ret == 0);
	*/
	return (0);
}
