/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: omykolai <omykolai@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/30 12:55:34 by omykolai          #+#    #+#             */
/*   Updated: 2017/12/07 18:43:37 by omykolai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include "get_next_line.h"

int main(void)
{
	char *line;
	
	if (1)
	{
		int fd1 = open("text1", O_RDONLY);
		int fd2 = open("text2", O_RDONLY);
		int fd3 = open("text3", O_RDONLY);
		int	fd4 = open("text", O_RDONLY);
		int fdt1 = open("temp1.txt", O_CREAT | O_RDWR);
		int fdt2 = open("temp2.txt", O_CREAT | O_RDWR);
		dprintf(fdt1, "%d\n", get_next_line(-15, &line));
		dprintf(fdt1, "%d\n", get_next_line(1, NULL));

		for (int i = 0; i < 32; ++i)
		{
			int red;
			if ((red = get_next_line(fd1, &line)) == 1)
				dprintf(fdt1, "%d\n%s\n", i + 1, line);
			else
				dprintf(fdt1, "%d\n", red);
			ft_memdel((void **)&line);
			if ((red = get_next_line(fd2, &line)) == 1)
				dprintf(fdt1, "%s\n", line);
			else
				dprintf(fdt1, "%d\n", red);
			ft_memdel((void **)&line);
			if ((red = get_next_line(fd3, &line)) == 1)
				dprintf(fdt1, "%s\n\n", line);
			else
				dprintf(fdt1, "%d\n\n", red);
			ft_memdel((void **)&line);
		}
		for (int i = 0; i < 10; ++i)
		{
			int red;
			if ((red = get_next_line(fd4, &line)) == 1)
				dprintf(fdt2, "%d\n%s\n", i + 1, line);
			else
				dprintf(fdt2, "%d\n", red);
			ft_memdel((void **)&line);
		}
		close(fd1);
		close(fd2);
		close(fd3);
		close(fd4);
		close(fdt1);
		close(fdt2);
	}
	else
		printf("WRONG ARGC\n");
	system("chmod 666 temp1.txt");
	system("chmod 666 temp2.txt");
	char c;
	system("echo \"diff1: \" && diff res1.txt temp1.txt");
	scanf("%c", &c);
	system("echo \"diff2: \" && diff res1.txt temp1.txt");
	scanf("%c", &c);
	system("rm temp1.txt");
	system("rm temp2.txt");
	system("leaks gnl | grep Process | grep leaks");
	return (0);
}
