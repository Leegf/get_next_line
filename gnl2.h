/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/06 18:26:28 by lburlach          #+#    #+#             */
/*   Updated: 2017/12/15 20:31:15 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H
# define BUFF_SIZE 5
# include "libft.h"
# include <fcntl.h>
# include <sys/types.h>
# include <sys/stat.h>

typedef	struct	s_ffs
{
	int 	fd;
	t_list	*st_list;
	struct	s_ffs *next;
	size_t	line_l;
	size_t	j;
}				t_ffs;

int		get_next_line(const int fd, char **line);

# define st_listt hm->st_list

#endif
