/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/18 19:20:54 by lburlach          #+#    #+#             */
/*   Updated: 2017/12/19 13:22:03 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include <stdio.h>

int		main(int argc, char **argv)
{
	int fd;
    int fd2;
	char *lolk;

	lolk = NULL;
	if (argc != 2)
		return (1);
	fd = open(argv[1], O_RDONLY);
	fd2 = open("temp.txt", O_CREAT | O_RDWR);
	while (get_next_line(fd, &lolk))
	{
        dprintf(fd2, "%s\n", lolk);
		ft_memdel((void **)&lolk);
	}
	return (0);
}
