# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/12/14 19:12:22 by lburlach          #+#    #+#              #
#    Updated: 2017/12/22 13:58:41 by lburlach         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

.PHONY: all clean fclean re

NAME = get_next_line
OBJ = get_next_line.o main.o
CC = gcc
CFLAGS = -Wall -Wextra -Werror
CURDIR = libft
HEAD = get_next_line.h
INC = $(addsuffix /includes, $(CURDIR))

all: $(NAME)
	$(MAKE) -C $(CURDIR)

$(NAME): $(OBJ)
	$(MAKE) -C $(CURDIR)
	$(CC) $(CFLAGS) -I $(INC) -o $@ $(CURDIR)/$(addsuffix .a, $(CURDIR)) $^

debug: $(OBJ)
	$(CC) $(CFLAGS) -g -I $(INC) main.c get_next_line.c $(CURDIR)/libft.a -o debug

libft.a: ololo
	$(MAKE) -C $(CURDIR)/

olol:
	true

clean:
	rm -f $(OBJ)
	$(MAKE) -C $(CURDIR) clean

fclean: clean
	rm -f $(NAME)
	$(MAKE) -C $(CURDIR) fclean

re: fclean all

$(OBJ) : %.o: %.c
	$(CC) -I $(INC) -c $(CFLAGS) $< -o $@

$(OBJ): $(HEAD)
