/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/28 16:29:13 by lburlach          #+#    #+#             */
/*   Updated: 2017/11/10 16:25:13 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** The strnstr() function locates the first occurrence of the null-terminated
** string needle in the string haystack,
** where not more than len characters are searched.
** Characters that appear after a `\0' character are not searched.
*/

char	*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	size_t k;
	size_t j;
	size_t n;

	k = 0;
	n = 0;
	if (*needle == '\0')
		return ((char *)haystack);
	while (haystack[k] && k < len)
	{
		j = 0;
		n = k;
		while (haystack[n] == needle[j] && n < len)
		{
			n++;
			j++;
			if (needle[j] == '\0')
				return ((char *)&haystack[k]);
		}
		k++;
	}
	return (NULL);
}
