/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isascii.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/28 17:53:04 by lburlach          #+#    #+#             */
/*   Updated: 2017/11/10 16:05:07 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** The isascii() function tests for an ASCII character, which is
** any character between 0 and octal 0177 inclusive.
*/

int	ft_isascii(int c)
{
	unsigned char tmp;

	if (c > 255 || c < 0)
		return (0);
	tmp = (unsigned char)c;
	if ((tmp > 0 && tmp <= 127) || tmp == 0)
		return (1);
	return (0);
}
