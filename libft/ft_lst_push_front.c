/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst_push_front.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/07 15:26:06 by lburlach          #+#    #+#             */
/*   Updated: 2017/12/12 16:32:49 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** It'll add an element into the beginning of a list.
*/

void	ft_lst_push_front(t_list **alst, void *content, size_t con_s)
{
	t_list *head;
	t_list *tmp;

	if (!alst)
		return ;
	head = *alst;
	tmp = ft_lstnew((void *)content, con_s);
	if (!(*alst))
	{
		*alst = tmp;
		return ;
	}
	tmp->next = head;
	*alst = tmp;
}
