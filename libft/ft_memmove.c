/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/29 13:52:30 by lburlach          #+#    #+#             */
/*   Updated: 2017/11/10 16:19:10 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** The memmove() function copies len bytes from string src to string
** dst.  The two strings may overlap; the copy is always done in a
** non-destructive manner.
*/

void			*ft_memmove(void *dst, const void *src, size_t len)
{
	size_t			i;
	unsigned char	*dst_c;
	unsigned char	*src_c;

	dst_c = (unsigned char *)dst;
	src_c = (unsigned char *)src;
	i = 0;
	if (src < dst && (src + len) > dst)
	{
		dst_c += len;
		src_c += len;
		while (len > 0)
		{
			*(--dst_c) = *(--src_c);
			len--;
		}
	}
	else
		while (i < len)
		{
			dst_c[i] = src_c[i];
			i++;
		}
	return (dst);
}
