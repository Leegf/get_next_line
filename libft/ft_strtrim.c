/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/30 20:02:00 by lburlach          #+#    #+#             */
/*   Updated: 2017/12/06 18:03:13 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Allocates (with malloc(3)) and returns a copy of the string given as
** argument without whitespaces at the beginning or at the end of the string.
** Will be considered as whitespaces the following characters ’ ’, ’\n’
** and ’\t’. If s has no whites- paces at the beginning or at the end,
** the function returns a copy of s. If the allocation fails the
** function returns NULL.
*/

char	*ft_strtrim(char const *s)
{
	size_t	i;
	size_t	j;
	size_t	len;
	char	*trimmed_str;

	i = 0;
	j = 0;
	if (s == NULL)
		return (NULL);
	while (*s == ' ' || *s == '\n' || *s == '\t')
		s++;
	len = ft_strlen(s);
	if (len == 0)
		return (ft_strdup(""));
	while (s[len - 1] == ' ' || s[len - 1] == '\n' || s[len - 1] == '\t')
	{
		j++;
		len--;
	}
	if ((trimmed_str = ft_strnew(len)) == NULL)
		return (NULL);
	i = -1;
	while (++i < len)
		trimmed_str[i] = s[i];
	return (trimmed_str);
}
