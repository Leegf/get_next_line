/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/30 20:56:12 by lburlach          #+#    #+#             */
/*   Updated: 2017/12/06 18:02:38 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Allocates (with malloc(3)) and returns an array of “fresh” strings
** (all ending with ’\0’, including the array itself) ob- tained by
** spliting s using the character c as a delimiter. If the allocation fails i
** the function returns NULL. Example : i
** ft_strsplit("*hello*fellow***students*", ’*’)
** returns the array ["hello", "fellow", "students"]
*/

static size_t		count_words(char const *str, char c)
{
	size_t i;

	i = 0;
	while (*str)
	{
		if (*str != c && (*(str + 1) == c || *(str + 1) == '\0'))
			i++;
		str++;
	}
	return (i);
}

static int			check_null(char **chk, size_t i)
{
	if (chk[i] == NULL)
	{
		while (i > 0)
			free(chk[i--]);
		free(chk[i]);
		free(chk);
		return (1);
	}
	else
		return (0);
}

static char			**allocation(char const *str, char c)
{
	size_t	i;
	size_t	j;
	char	**res;

	i = -1;
	res = (char **)malloc(sizeof(char *) * count_words(str, c) + 1);
	if (res != NULL)
		while (*str)
		{
			j = 0;
			if (*str != c && *str != '\0')
			{
				while (*str != c && *str != '\0')
				{
					j++;
					str++;
				}
				str -= 1;
				res[++i] = (char *)malloc(sizeof(char) * j + 1);
				if (check_null(res, i))
					return (NULL);
			}
			str++;
		}
	return (res);
}

static char			**filling(char **res, char const *str, char c)
{
	size_t	i;
	size_t	j;

	i = 0;
	while (*str)
	{
		j = 0;
		if (*str != c && *str != '\0')
		{
			while (*str != c && *str != '\0')
			{
				res[i][j++] = *str;
				str++;
			}
			str -= 1;
			res[i][j] = '\0';
			i++;
		}
		str++;
	}
	res[i] = 0;
	return (res);
}

char				**ft_strsplit(char const *s, char c)
{
	char **res;

	if (s == NULL)
		return (NULL);
	while (*s == c)
		s++;
	res = allocation(s, c);
	if (res == NULL)
		return (NULL);
	res = filling(res, s, c);
	return (res);
}
