/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isdigit.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/28 17:36:55 by lburlach          #+#    #+#             */
/*   Updated: 2017/11/09 19:32:08 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** The isdigit() function tests for a decimal digit character.
** Regardless of locale, this includes the following
** characters only:
** ``0''         ``1''         ``2''         ``3''         ``4''
** ``5''         ``6''         ``7''         ``8''         ``9''
*/

int		ft_isdigit(int c)
{
	unsigned char tmp;

	if (c > 255 || c < 0)
		return (0);
	tmp = (unsigned char)c;
	if (tmp >= '0' && tmp <= '9')
		return (1);
	return (0);
}
