/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst_at.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/07 15:29:41 by lburlach          #+#    #+#             */
/*   Updated: 2017/12/06 18:05:31 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** It'll return a list element at nbr'th place.
*/

t_list	*ft_lst_at(t_list *lst, size_t nbr)
{
	size_t i;

	if (!lst)
		return (NULL);
	i = 0;
	while (i < nbr && lst)
	{
		lst = lst->next;
		i++;
	}
	return (lst);
}
